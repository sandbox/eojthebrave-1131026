/**
 * @file
 * Javascript for chaptermarkers module.
 */

/**
 * JWPlayer chapter marker behavior.
 */
Drupal.behaviors.chaptermarkersJWPLayer = function(context) {
  // If there is a #time in the current href jump to that point in the
  // video.
  var href = window.location.href;
  var fragment = href.slice(href.indexOf('#') + 1).split(':');

  if (fragment[0] && parseInt(fragment[1])) {
    Drupal.chaptermarkersJWPLayerJumpTo(fragment[0], fragment[1]);
  }

  // Find all the chapter marker links.
  $('a.chaptermarkers-marker').click(function() {
    var href = $(this).attr('href');
    var fragment = href.slice(href.indexOf('#') + 1).split(':');

    if (parseInt(fragment[1])) {
      Drupal.chaptermarkersJWPLayerJumpTo(fragment[0], fragment[1]);
    }

    // Set the browser's URL so people can link directly to this chapter.
    window.location = href;
    return false;
  });
};

/**
 * Tell the video player to jump to the specified time within the current video.
 *
 * @param player
 *   Player DOM object.
 * @param time
 *   Time in milliseconds to jump to.
 */
Drupal.chaptermarkersJWPLayerJumpTo = function(player, time) {
  $player = $('#' + player);
  // Jump the player to the appropriate time.
  // The time variable is converted to seconds here.
  jwplayer($player.id).seek(time/1000);
};
